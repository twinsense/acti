<?php 

get_header(); 

	$id = get_queried_object_id();
	$meta_filter = array('relation'	=> 'AND');

	if($id==4){

		if (!empty($_POST)){

			if(isset($_POST['post_per_page'])){
				$_SESSION['post_per_page'] = $_POST['post_per_page'];
			}else{
				$_SESSION['post_per_page'] = 12;
			}

			if(isset($_POST['resolutie'])){
	
				$_SESSION['resolutie'] = $_POST['resolutie'];
				foreach($_POST['resolutie'] as $val){
					$meta_filter[] = array('key' =>'resolutie','value' => $val,'compare' => 'LIKE');
				}
	
			}else{
				unset($_SESSION['resolutie']);
			}

			if(isset($_POST['lens'])){
				$_SESSION['lens'] = $_POST['lens'];
				foreach($_POST['lens'] as $val){
					$meta_filter[] = array('key' =>'lens','value' => $val,'compare' => 'LIKE');
				}
			}else{
				unset($_SESSION['lens']);
			}

			if(isset($_POST['type'])){
				$_SESSION['type'] = $_POST['type'];
				foreach($_POST['type'] as $val){
					$meta_filter[] = array('key' =>'type','value' => $val,'compare' => 'LIKE');
				}
			}else{
				unset($_SESSION['type']);
			}

			if(isset($_POST['wdr'])){
				$_SESSION['wdr'] = $_POST['wdr'];
				foreach($_POST['wdr'] as $val){
					$meta_filter[] = array('key' =>'wdr','value' => $val,'compare' => 'LIKE');
				}
			}else{
				unset($_SESSION['wdr']);
			}

			if(isset($_POST['kijkhoek'])){
				$_SESSION['kijkhoek'] = $_POST['kijkhoek'];
				foreach($_POST['kijkhoek'] as $val){
					$meta_filter[] = array('key' =>'kijkhoek','value' => $val,'compare' => 'LIKE');
				}
			}else{
				unset($_SESSION['kijkhoek']);
			}

			if(isset($_POST['sensitivity'])){
				$_SESSION['sensitivity'] = $_POST['sensitivity'];
				foreach($_POST['sensitivity'] as $val){
					$meta_filter[] = array('key' =>'sensitivity','value' => $val,'compare' => 'LIKE');
				}
			}else{
				unset($_SESSION['sensitivity']);
			}

			if(isset($_POST['irled'])){
				$_SESSION['irled'] = $_POST['irled'];
				foreach($_POST['irled'] as $val){
					$meta_filter[] = array('key' =>'ir_led','value' => $val,'compare' => 'LIKE');
				}
			}else{
				unset($_SESSION['irled']);
			}

			if(isset($_POST['opties'])){
				$_SESSION['opties'] = $_POST['opties'];
				foreach($_POST['opties'] as $val){
					$meta_filter[] = array('key' =>'opties','value' => $val,'compare' => 'LIKE');
				}
			}else{
				unset($_SESSION['opties']);
			}

		}else{

			if(isset($_POST['post_per_page'])){
				$_SESSION['post_per_page'] = $_POST['post_per_page'];
			}else{
				$_SESSION['post_per_page'] = 12;
			}

			if(!empty($_SESSION['resolutie'])){
				foreach($_SESSION['resolutie'] as $val){
					$meta_filter[] = array('key' =>'resolutie','value' => $val,'compare' => 'LIKE');
				}
			}
			if(!empty($_SESSION['lens'])){
				foreach($_SESSION['lens'] as $val){
					$meta_filter[] = array('key' =>'lens','value' => $val,'compare' => 'LIKE');
				}
			}
			if(!empty($_SESSION['type'])){
				foreach($_SESSION['type'] as $val){
					$meta_filter[] = array('key' =>'type','value' => $val,'compare' => 'LIKE');
				}
			}
			if(!empty($_SESSION['wdr'])){
				foreach($_SESSION['wdr'] as $val){
					$meta_filter[] = array('key' =>'wdr','value' => $val,'compare' => 'LIKE');
				}
			}
			if(!empty($_SESSION['kijkhoek'])){
				foreach($_SESSION['kijkhoek'] as $val){
					$meta_filter[] = array('key' =>'kijkhoek','value' => $val,'compare' => 'LIKE');
				}
			}
			if(!empty($_SESSION['sensitivity'])){
				foreach($_SESSION['sensitivity'] as $val){
					$meta_filter[] = array('key' =>'sensitivity','value' => $val,'compare' => 'LIKE');
				}
			}
			if(!empty($_SESSION['irled'])){
				foreach($_SESSION['irled'] as $val){
					$meta_filter[] = array('key' =>'ir_led','value' => $val,'compare' => 'LIKE');
				}
			}
			if(!empty($_SESSION['opties'])){
				foreach($_SESSION['opties'] as $val){
					$meta_filter[] = array('key' =>'opties','value' => $val,'compare' => 'LIKE');
				}
			}
		}
	}
	if($id==5){
		if (!empty($_POST)){

			if(isset($_POST['post_per_page'])){
				$_SESSION['post_per_page'] = $_POST['post_per_page'];
			}else{
				$_SESSION['post_per_page'] = 12;
			}

			if(isset($_POST['aantal_kanalen'])){
				$_SESSION['aantal_kanalen'] = $_POST['aantal_kanalen'];
				foreach($_POST['aantal_kanalen'] as $val){
					$meta_filter[] = array('key' =>'aantal_kanalen','value' => $val,'compare' => 'LIKE');
				}
			}else{
				unset($_SESSION['aantal_kanalen']);
			}

			if(isset($_POST['opties_nvr'])){
				$_SESSION['opties_nvr'] = $_POST['opties_nvr'];
				foreach($_POST['opties_nvr'] as $val){
					$meta_filter[] = array('key' =>'opties','value' => $val,'compare' => 'LIKE');
				}
			}else{
				unset($_SESSION['opties_nvr']);
			}

			if(isset($_POST['hdd_slots'])){
				$_SESSION['hdd_slots'] = $_POST['hdd_slots'];
				foreach($_POST['hdd_slots'] as $val){
					$meta_filter[] = array('key' =>'hdd_slots','value' => $val,'compare' => 'LIKE');
				}
			}else{
				unset($_SESSION['hdd_slots']);
			}
		}else{

			if(isset($_POST['post_per_page'])){
				$_SESSION['post_per_page'] = $_POST['post_per_page'];
			}else{
				$_SESSION['post_per_page'] = 12;
			}

			if(!empty($_SESSION['aantal_kanalen'])){
				foreach($_SESSION['aantal_kanalen'] as $val){
					$meta_filter[] = array('key' =>'aantal_kanalen','value' => $val,'compare' => 'LIKE');
				}
			}
			if(!empty($_SESSION['opties_nvr'])){
				foreach($_SESSION['opties_nvr'] as $val){
					$meta_filter[] = array('key' =>'opties','value' => $val,'compare' => 'LIKE');
				}
			}
			if(!empty($_SESSION['hdd_slots'])){
				foreach($_SESSION['hdd_slots'] as $val){
					$meta_filter[] = array('key' =>'hdd_slots','value' => $val,'compare' => 'LIKE');
				}
			}
		}
	}

	if($id==6){
		if (!empty($_POST)){
			if(isset($_POST['post_per_page'])){
				$_SESSION['post_per_page'] = $_POST['post_per_page'];
			}else{
				$_SESSION['post_per_page'] = 12;
			}

			if(isset($_POST['geschikt_voor'])){
				$_SESSION['geschikt_voor'] = $_POST['geschikt_voor'];
				foreach($_POST['geschikt_voor'] as $val){
					$meta_filter[] = array('key' =>'geschikt_voor','value' => $val,'compare' => 'LIKE');
				}
			}else{
				unset($_SESSION['geschikt_voor']);
			}

			if(isset($_POST['soort'])){
				$_SESSION['soort'] = $_POST['soort'];
				foreach($_POST['soort'] as $val){
					$meta_filter[] = array('key' =>'soort','value' => $val,'compare' => 'LIKE');
				}
			}else{
				unset($_SESSION['soort']);
			}

			if(isset($_POST['type'])){
				$_SESSION['type'] = $_POST['type'];
				foreach($_POST['type'] as $val){
					$meta_filter[] = array('key' =>'type','value' => $val,'compare' => 'LIKE');
				}
			}else{
				unset($_SESSION['type']);
			}
		}else{

			if(isset($_POST['post_per_page'])){
				$_SESSION['post_per_page'] = $_POST['post_per_page'];
			}else{
				$_SESSION['post_per_page'] = 12;
			}

			if(!empty($_SESSION['geschikt_voor'])){
				foreach($_SESSION['geschikt_voor'] as $val){
					$meta_filter[] = array('key' =>'geschikt_voor','value' => $val,'compare' => 'LIKE');
				}
			}
			if(!empty($_SESSION['soort'])){
				foreach($_SESSION['soort'] as $val){
					$meta_filter[] = array('key' =>'soort','value' => $val,'compare' => 'LIKE');
				}
			}
			if(!empty($_SESSION['type'])){
				foreach($_SESSION['type'] as $val){
					$meta_filter[] = array('key' =>'type','value' => $val,'compare' => 'LIKE');
				}
			}
		}
	}

	if(!empty($_POST)){
		if(isset($_POST['reset_filter'])){
			session_unset();
			$_SESSION['post_per_page'] = 12;
			$meta_filter = array('relation'	=> 'AND');
		}
	}

	//print_r($meta_filter);

?>

<div class="content">

	<div class="container">
		<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
			<?php if(function_exists('bcn_display')){
				bcn_display();
			}?>
		</div>
	</div>

	<div class="section grey">

		<div class="graph">
			<?php echo file_get_contents(get_template_directory_uri()."/images/section_graph.svg");?>
		</div>

		<div class="container">
		
			<h1><?php single_term_title(); ?></h1>

			<div class="row">
				<div class="col-md-4 col-lg-3">

					<div class="filter">

						<div class="btn full-width btn-primary filter-expander">Filters</div>

						<div class="filter-sections">

							<form id="product-filter" method="post" action="<?php echo get_term_link($id); ?>">

								<div class="filter-section">
									<h4>Weergeven per pagina:</h4>
									<select class="form-control" name="post_per_page">
										<option value="12"<?php if($_SESSION['post_per_page']==12){echo ' selected';} ?>>12</option>
										<option value="24"<?php if($_SESSION['post_per_page']==24){echo ' selected';} ?>>24</option>
										<option value="36"<?php if($_SESSION['post_per_page']==36){echo ' selected';} ?>>36</option>
									</select>
								</div>

								<?php 
									// Camera's

									$resolutie = get_field_object('field_605369b2f583b');
									$lens = get_field_object('field_60536ca765ac6');
									$type = get_field_object('field_6078251aa78b3');
									$wdr = get_field_object('field_60536ce465ac7');
									$kijkhoek = get_field_object('field_60536dc578e37');
									$sensitivity = get_field_object('field_60536ffa78e39');
									$irled = get_field_object('field_6053703578e3a');
									$opties = get_field_object('field_6053705f78e3c');

									$resolutie_choices = $resolutie['choices'];
									$lens_choices = $lens['choices'];
									$type_choices = $type['choices'];
									$wdr_choices = $wdr['choices'];
									$kijkhoek_choices = $kijkhoek['choices'];
									$sensitivity_choices = $sensitivity['choices'];
									$irled_choices = $irled['choices'];
									$opties_choices = $opties['choices'];

									//NVR's

									$aantal_kanalen = get_field_object('field_605a00e298027');
									$opties_nvr = get_field_object('field_605a00e29bab9');
									$hdd_slots = get_field_object('field_605a00e29f53b');

									$aantal_kanalen_choices = $aantal_kanalen['choices'];
									$opties_nvr_choices = $opties_nvr['choices'];
									$hdd_slots_choices = $hdd_slots['choices'];

									//Acc

									$geschikt_voor = get_field_object('field_605a025de1d33');
									//$soort = get_field_object('field_6061f690c61bb');
									//$type = get_field_object('field_6061f706c61bc');

									$geschikt_voor_choices = $geschikt_voor['choices'];
									//$soort_choices = $soort['choices'];
									//$type_choices = $type['choices'];

								?>
								<?php if($id==4){ ?>
									<div class="filter-section">
										<h4>Resolutie</h4>
										<?php 
											foreach( $resolutie_choices as $k => $v ){
												if(!empty($_SESSION['resolutie'])){
													if(in_array( $k, $_SESSION['resolutie'])==true){
														$checked1 = ' checked';
													}else{
														$checked1 = '';
													}
												}
												echo '<div class="form-check">';
												echo '<input class="form-check-input" type="checkbox" name="resolutie[]" value="'. $k .'"'.$checked1.'>';
												echo '<label class="form-check-label">'.$v.'</label>';
												echo '</div>';
											}
										?>
									</div>

									<div class="filter-section">
										<h4>Type</h4>
										<?php 
											foreach( $type_choices as $k => $v ){
												if(!empty($_SESSION['type'])){
													if(in_array( $k, $_SESSION['type'])==true){
														$checked2a = ' checked';
													}else{
														$checked2a = '';
													}
												}
												echo '<div class="form-check">';
												echo '<input class="form-check-input" type="checkbox" name="type[]" value="'. $k .'"'.$checked2a.'>';
												echo '<label class="form-check-label">'.$v.'</label>';
												echo '</div>';
											}
										?>
									</div>

									<div class="filter-section">
										<h4>Lens</h4>
										<?php 
											foreach( $lens_choices as $k => $v ){
												if(!empty($_SESSION['lens'])){
													if(in_array( $k, $_SESSION['lens'])==true){
														$checked2 = ' checked';
													}else{
														$checked2 = '';
													}
												}
												echo '<div class="form-check">';
												echo '<input class="form-check-input" type="checkbox" name="lens[]" value="'. $k .'"'.$checked2.'>';
												echo '<label class="form-check-label">'.$v.'</label>';
												echo '</div>';
											}
										?>
									</div>

									<div class="filter-section">
										<h4>WDR</h4>
										<?php 
											foreach( $wdr_choices as $k => $v ){
												if(!empty($_SESSION['wdr'])){
													if(in_array( $k, $_SESSION['wdr'])==true){
														$checked3 = ' checked';
													}else{
														$checked3 = '';
													}
												}
												echo '<div class="form-check">';
												echo '<input class="form-check-input" type="checkbox" name="wdr[]" value="'. $k .'"'.$checked3.'>';
												echo '<label class="form-check-label">'.$v.'</label>';
												echo '</div>';
											}
										?>
									</div>

									<div class="filter-section">
										<h4>Kijkhoek</h4>
										<?php 
											foreach( $kijkhoek_choices as $k => $v ){
												if(!empty($_SESSION['kijkhoek'])){
													if(in_array( $k, $_SESSION['kijkhoek'])==true){
														$checked4 = ' checked';
													}else{
														$checked4 = '';
													}
												}
												echo '<div class="form-check">';
												echo '<input class="form-check-input" type="checkbox" name="kijkhoek[]" value="'. $k .'"'.$checked4.'>';
												echo '<label class="form-check-label">'.$v.'</label>';
												echo '</div>';
											}
										?>
									</div>

									<div class="filter-section">
										<h4>Sensitivity</h4>
										<?php 
											foreach( $sensitivity_choices as $k => $v ){
												if(!empty($_SESSION['sensitivity'])){
													if(in_array( $k, $_SESSION['sensitivity'])==true){
														$checked5 = ' checked';
													}else{
														$checked5 = '';
													}
												}
												echo '<div class="form-check">';
												echo '<input class="form-check-input" type="checkbox" name="sensitivity[]" value="'. $k .'"'.$checked5.'>';
												echo '<label class="form-check-label">'.$v.'</label>';
												echo '</div>';
											}
										?>
									</div>
									
									<div class="filter-section">
										<h4>IR LED</h4>
										<?php 
											foreach( $irled_choices as $k => $v ){
												if(!empty($_SESSION['irled'])){
													if(in_array( $k, $_SESSION['irled'])==true){
														$checked6 = ' checked';
													}else{
														$checked6 = '';
													}
												}
												echo '<div class="form-check">';
												echo '<input class="form-check-input" type="checkbox" name="irled[]" value="'. $k .'"'.$checked6.'>';
												echo '<label class="form-check-label">'.$v.'</label>';
												echo '</div>';
											}
										?>
									</div>
									
									<div class="filter-section">
										<h4>Opties</h4>
										<?php 
											foreach( $opties_choices as $k => $v ){
												if(!empty($_SESSION['opties'])){
													if(in_array( $k, $_SESSION['opties'])==true){
														$checked7 = ' checked';
													}else{
														$checked7 = '';
													}
												}
												echo '<div class="form-check">';
												echo '<input class="form-check-input" type="checkbox" name="opties[]" value="'. $k .'"'.$checked7.'>';
												echo '<label class="form-check-label">'.$v.'</label>';
												echo '</div>';
											}
										?>
									</div>
								<?php } ?>

								<?php if($id==5){ ?>
									<div class="filter-section">
										<h4>Aantal kanalen</h4>
										<?php 
											foreach( $aantal_kanalen_choices as $k => $v ){
												if(!empty($_SESSION['aantal_kanalen'])){
													if(in_array( $k, $_SESSION['aantal_kanalen'])==true){
														$checked8 = ' checked';
													}else{
														$checked8 = '';
													}
												}
												echo '<div class="form-check">';
												echo '<input class="form-check-input" type="checkbox" name="aantal_kanalen[]" value="'. $k .'"'.$checked8.'>';
												echo '<label class="form-check-label">'.$v.'</label>';
												echo '</div>';
											}
										?>
									</div>
									<div class="filter-section">
										<h4>Opties</h4>
										<?php 
											foreach( $opties_nvr_choices as $k => $v ){
												if(!empty($_SESSION['opties_nvr'])){
													if(in_array( $k, $_SESSION['opties_nvr'])==true){
														$checked9 = ' checked';
													}else{
														$checked9 = '';
													}
												}
												echo '<div class="form-check">';
												echo '<input class="form-check-input" type="checkbox" name="opties_nvr[]" value="'. $k .'"'.$checked9.'>';
												echo '<label class="form-check-label">'.$v.'</label>';
												echo '</div>';
											}
										?>
									</div>
									<div class="filter-section">
										<h4>Aantal HDD slots</h4>
										<?php 
											foreach( $hdd_slots_choices as $k => $v ){
												if(!empty($_SESSION['hdd_slots'])){
													if(in_array( $k, $_SESSION['hdd_slots'])==true){
														$checked10 = ' checked';
													}else{
														$checked10 = '';
													}
												}
												echo '<div class="form-check">';
												echo '<input class="form-check-input" type="checkbox" name="hdd_slots[]" value="'. $k .'"'.$checked10.'>';
												echo '<label class="form-check-label">'.$v.'</label>';
												echo '</div>';
											}
										?>
									</div>
								<?php } ?>

								<?php if($id==6){ ?>

									<div class="filter-section">
										<h4>Geschikt voor</h4>
										<?php 
											foreach( $geschikt_voor_choices as $k => $v ){
												if(!empty($_SESSION['geschikt_voor'])){
													if(in_array( $k, $_SESSION['geschikt_voor'])==true){
														$checked11 = ' checked';
													}else{
														$checked11 = '';
													}
												}
												echo '<div class="form-check">';
												echo '<input class="form-check-input" type="checkbox" name="geschikt_voor[]" value="'. $k .'"'.$checked11.'>';
												echo '<label class="form-check-label">'.$v.'</label>';
												echo '</div>';
											}
										?>
									</div>
									
								<?php } ?>

								<button type="submit" name="reset_filter" class="btn full-width btn-primary">Reset filters</button>

							</form>
						</div>

					</div>

				</div>
				<div class="col-md-8 col-lg-9">

					<?php //var_dump($meta_filter); ?>

					<?php
						$args = array (
							'posts_per_page' => $_SESSION['post_per_page'],
							'paged' => $paged,
							'orderby' => 'menu_order',
							'order' => 'ASC',
							'post_type' => 'product',
							'post_status' => 'publish',
							'tax_query' => array(
								'relation' => 'AND',
								array(
									'taxonomy' => 'product_cat',
									'field' => 'term_id',
									'terms' => $id
								),
							),
							'meta_query' => $meta_filter
						);
					?>

					<?php 
						if ( get_query_var( 'paged' ) ) { $paged = get_query_var( 'paged' ); }
						elseif ( get_query_var( 'page' ) ) { $paged = get_query_var( 'page' ); }
						else { $paged = 1; } 
					?>

					<?php query_posts($args); ?>
            		<?php if(have_posts()): ?>
                		<div id="product-grid" class="row">
                    		<?php while(have_posts()): the_post(); ?>

								<div class="col-12 col-md-6 col-lg-4">
									
									<?php //var_dump(get_field('resolutie')); ?>
									<?php $attachment_image = wp_get_attachment_url( get_post_thumbnail_id(), 'gallery' ); ?>
							
									<a href="<?php echo get_the_permalink(); ?>" class="product-item">
										<div class="inner">
											<div class="product-item-image" style="background-image:url(<?php echo $attachment_image; ?>);">
											</div>
											<h4><?php the_title(); ?></h4>
										</div>
									</a>
								</div>

							<?php endwhile; ?>
						</div>
						<div class="pagination-holder">
							<?php
								if (function_exists("fellowtuts_wpbs_pagination"))
								{
									fellowtuts_wpbs_pagination();
									//fellowtuts_wpbs_pagination($the_query->max_num_pages);
								}
							?>
						</div>
					<?php else : ?>
						<p>Geen producten gevonden!</p>
					<?php endif; wp_reset_query(); ?>
				</div>
			</div>
		</div>		
	</div>

	<?php $cat = get_queried_object(); ?>

	<?php if( have_rows('secties', $cat) ): ?>
		<?php while ( have_rows('secties', $cat) ) : the_row(); ?>
			<?php include('blocks/sections.php'); ?>
		<?php endwhile; ?>	
	<?php endif; ?>	

</div>

<?php get_footer(); ?>
