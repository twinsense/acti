	
	<footer class="footer">

		<div class="graph">
			<?php echo file_get_contents(get_template_directory_uri()."/images/section_graph.svg");?>
		</div>

		<div class="footer-top">
	  		<div class="container">
				<div class="row">
					<div class="col-md-6 col-lg-3 footer-section-1">
						<?php dynamic_sidebar('footer1'); ?>
					</div>
					<div class="col-md-6 col-lg-3 footer-section-2">
						<?php dynamic_sidebar('footer2'); ?>
					</div>
					<div class="col-md-6 col-lg-3 footer-section-3">
						<?php dynamic_sidebar('footer3'); ?>
					</div>
					<div class="col-md-6 col-lg-3 footer-section-4">
						<?php dynamic_sidebar('footer4'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="footer-bottom">
			<div class="container">
				<p>Copyright Xpert Data b.v. <?php echo date('Y'); ?> | gegevens, foto’s of specificaties kunnen afwijken, wijzigingen voorbehouden.</p>
			</div>
		</div>
	</footer>
	
	<?php wp_footer(); ?>
	</body>
</html>
