// Webpack uses this to work with directories
const path = require('path');
var webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const SpriteLoaderPlugin = require('svg-sprite-loader/plugin');

// This is the main configuration object.
// Here you write different options and tell Webpack what to do
module.exports = {
  // Path to your entry point(s). From this file Webpack will begin his work
  entry: {
    frontend: ['./src/index.js']
  },

  // Path and filename of your result bundle.
  // Webpack will bundle all JavaScript into this file
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: '[name]-bundle.js'
  },

  // Default mode for Webpack is production.
  // Depending on mode Webpack will apply different things
  // on final bundle. In Bitbucket Pipeline the mode is set to production
  mode: 'development',
  module: {
    rules: [
      // Eslint loader
      {
        // enforce: 'pre',
        exclude: /node_modules/,
        test: /\.jsx$/,
        loader: 'eslint-loader'
      },

      // Babel loader
      {
        test: /(\.jsx|\.js)$/, // JSX and JS files should be present.
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },

      // CSS & SASS loader
      {
        test: /\.(sa|sc|c)ss$/, // Apply rule for .sass, .scss or .css files
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
      },

      // URL loader
      {
        test: /\.(eot|woff|woff2|ttf|svg|png|jpg|gif)$/,
        loader: 'url-loader?limit=30000&name=[name]-[hash].[ext]'
      },

      // File loader
      {
        test: /\.(jpe?g|png|gif)\$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              outputPath: 'images/',
              name: '[name].[ext]'
            }
          },
          'img-loader'
        ]
      }
    ]
  },

  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        sourceMap: true
      }),
      new OptimizeCssAssetsPlugin({
        cssProcessorOptions: {
          map: {
            inline: false
          }
        }
      })
    ]
  },

  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery'
    }),
    // new StyleLintPlugin(),
    new MiniCssExtractPlugin({ filename: '[name].css' }),
    new SpriteLoaderPlugin(),
    new UglifyJsPlugin({
      sourceMap: true,
      parallel: true,
      cache: true
    }),
    new webpack.SourceMapDevToolPlugin({
      filename: '[file].map',
      exclude: /node_modules\/(?!(bootstrap)\/).*/
    }),
    new BrowserSyncPlugin({
      files: '**/*.php',
      injectChanges: true,
      proxy: 'acti.local'
    })
  ]
};
