<?php if( have_rows('secties') ): ?>
    <?php while ( have_rows('secties') ) : the_row(); ?>
    
        <?php if( get_row_layout() == '1_kolom' ): ?>
            
            <?php
                $narrow = get_sub_field('smalle_weergave');
                    
                if($narrow){
                    $narrow_open = '<div class="row justify-content-center"><div class="col-12 col-md-10">';
                    $narrow_close = '</div></div>';
                }else{
                    $narrow_open = '';
                    $narrow_close = '';
                }
                $bg = get_sub_field('stijl'); 
            ?>
        
            <div class="section <?php echo $bg; ?>">

                <div class="graph">
                    <?php echo file_get_contents(get_template_directory_uri()."/images/section_graph.svg");?>
                </div>

                <div class="container">
                
                    <?php if(get_sub_field('titel')){ ?>
                        <h1 class="section-title"><?php the_sub_field('titel'); ?></h1>
                    <?php } ?>

                    <?php echo $narrow_open; ?>
                        <?php include('main-fields.php'); ?>
                    <?php echo $narrow_close; ?>

                </div>
            </div>
        
        <?php elseif( get_row_layout() == '2_kolom' ): ?>
            
            <?php

                $narrow = get_sub_field('smalle_weergave');

                if($narrow){
                    $narrow_open = '<div class="row justify-content-center"><div class="col-12 col-md-10">';
                    $narrow_close = '</div></div>';
                }else{
                    $narrow_open = '';
                    $narrow_close = '';
                }

                $bg = get_sub_field('stijl');
                $columns = get_sub_field('kolomverhouding');
    
                if($columns=='3/9'){
                    $col1 = 'col-md-6 col-lg-4 col-xl-3';
                    $col2 = 'col-md-6 col-lg-8 col-xl-9';
                }elseif($columns=='4/8'){
                    $col1 = 'col-md-6 col-lg-4';
                    $col2 = 'col-md-6 col-lg-8';
                }elseif($columns=='5/7'){
                    $col1 = 'col-md-6 col-lg-5';
                    $col2 = 'col-md-6 col-lg-7';
                }elseif($columns=='6/6'){
                    $col1 = 'col-md-6';
                    $col2 = 'col-md-6';
                }elseif($columns=='7/5'){
                    $col1 = 'col-md-6 col-lg-7';
                    $col2 = 'col-md-6 col-lg-5';
                }elseif($columns=='8/4'){
                    $col1 = 'col-md-6 col-lg-8';
                    $col2 = 'col-md-6 col-lg-4';
                }else{
                    $col1 = 'col-md-6 col-lg-8 col-xl-9';
                    $col2 = 'col-md-6 col-lg-4 col-xl-3';
                }
            ?>
            
            <div class="section <?php echo $bg; ?>">

                <div class="graph">
                    <?php echo file_get_contents(get_template_directory_uri()."/images/section_graph.svg");?>
                </div>
                
                <div class="container">
                
                    <?php if(get_sub_field('titel')){ ?>
                        <h1 class="section-title"><?php the_sub_field('titel'); ?></h1>
                    <?php } ?>

                    <?php echo $narrow_open; ?>
                        <div class="row">
                            <div class="<?php echo $col1; ?> align-self-center">
                                <?php if( have_rows('links') ): ?>
                                    <?php while( have_rows('links') ): the_row(); ?>
                                        <?php include('main-fields.php'); ?>
                                    <?php endwhile; ?>
                                <?php endif; ?>
                            </div>
                            <div class="<?php echo $col2; ?> align-self-center">
                                <?php if( have_rows('rechts') ): ?>
                                    <?php while( have_rows('rechts') ): the_row(); ?>
                                        <?php include('main-fields.php'); ?>
                                    <?php endwhile; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php echo $narrow_close; ?>

                </div>
            </div>
                
        <?php endif; ?>
        
    <?php endwhile; ?>
    
<?php else : echo '<div class="container"><p>Nog geen inhoud toegevoegd op pagina.</p></div>'; ?>
    
<?php endif; ?>