<?php if( get_row_layout() == '1_kolom' ): ?>
				
    <?php
        $narrow = get_sub_field('smalle_weergave');
            
        if($narrow){
            $narrow_open = '<div class="row justify-content-center"><div class="col-12 col-md-10">';
            $narrow_close = '</div></div>';
        }else{
            $narrow_open = '';
            $narrow_close = '';
        }
        $bg = get_sub_field('stijl');
        $bgimage = get_sub_field('achtergrond');

        if($bgimage){
            $whitebox_open = '<div class="whitebox">';
            $whitebox_close = '</div>';
        }else{
            $whitebox_open = '';
            $whitebox_close = '';
        }
    ?>

    <div class="section <?php echo $bg; ?>" <?php if($bgimage){ echo 'style="background-image:url('.$bgimage['sizes']['header'].');"'; } ?>>

        <div class="graph">
            <?php echo file_get_contents(get_template_directory_uri()."/images/section_graph.svg");?>
        </div>

        <div class="container">
            <?php echo $whitebox_open; ?>
                <?php if(get_sub_field('titel')){ ?>
                    <?php 
                        $uitl = get_sub_field('uitlijning'); 
                        if($uitl){
                            $txt = ' text-'.$uitl;
                        }else{
                            $txt = ' text-center';
                        }
                    ?>
                    <h1 class="section-title<?php echo $txt; ?>"><?php the_sub_field('titel'); ?></h1>
                <?php } ?>

                <?php echo $narrow_open; ?>
                    <?php include('main-fields.php'); ?>
                <?php echo $narrow_close; ?>
            <?php echo $whitebox_close; ?>
        </div>
    </div>

<?php elseif( get_row_layout() == '2_kolom' ): ?>
    
    <?php

        $narrow = get_sub_field('smalle_weergave');

        if($narrow){
            $narrow_open = '<div class="row justify-content-center"><div class="col-12 col-md-10">';
            $narrow_close = '</div></div>';
        }else{
            $narrow_open = '';
            $narrow_close = '';
        }

        $bg = get_sub_field('stijl');
        $bgimage = get_sub_field('achtergrond');
        $columns = get_sub_field('kolomverhouding');
        $cont_uitl = get_sub_field('content_uitlijning');

        if($cont_uitl){
            $align = $cont_uitl;
        }else{
            $align = 'center';
        }

        if($bgimage){
            $whitebox_open = '<div class="whitebox">';
            $whitebox_close = '</div>';
        }else{
            $whitebox_open = '';
            $whitebox_close = '';
        }

        if($columns=='3/9'){
            $col1 = 'col-md-6 col-lg-4 col-xl-3';
            $col2 = 'col-md-6 col-lg-8 col-xl-9';
        }elseif($columns=='4/8'){
            $col1 = 'col-md-6 col-lg-4';
            $col2 = 'col-md-6 col-lg-8';
        }elseif($columns=='5/7'){
            $col1 = 'col-md-6 col-lg-5';
            $col2 = 'col-md-6 col-lg-7';
        }elseif($columns=='6/6'){
            $col1 = 'col-md-6';
            $col2 = 'col-md-6';
        }elseif($columns=='7/5'){
            $col1 = 'col-md-6 col-lg-7';
            $col2 = 'col-md-6 col-lg-5';
        }elseif($columns=='8/4'){
            $col1 = 'col-md-6 col-lg-8';
            $col2 = 'col-md-6 col-lg-4';
        }else{
            $col1 = 'col-md-6 col-lg-8 col-xl-9';
            $col2 = 'col-md-6 col-lg-4 col-xl-3';
        }
    ?>
    
    <div class="section <?php echo $bg; ?>" <?php if($bgimage){ echo 'style="background-image:url('.$bgimage['sizes']['header'].');"'; } ?>>

        <div class="graph">
            <?php echo file_get_contents(get_template_directory_uri()."/images/section_graph.svg");?>
        </div>
        
        <div class="container">
            <?php echo $whitebox_open; ?>
                <?php if(get_sub_field('titel')){ ?>
                    <?php 
                        $uitl = get_sub_field('uitlijning'); 
                        if($uitl){
                            $txt = ' text-'.$uitl;
                        }else{
                            $txt = ' text-center';
                        }
                    ?>
                    <h1 class="section-title<?php echo $txt; ?>"><?php the_sub_field('titel'); ?></h1>
                <?php } ?>

                <?php echo $narrow_open; ?>
                    <div class="row">
                        <div class="<?php echo $col1; ?> align-self-<?php echo $align; ?>">
                            <?php if( have_rows('links') ): ?>
                                <?php while( have_rows('links') ): the_row(); ?>
                                    <?php include('main-fields.php'); ?>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                        <div class="<?php echo $col2; ?> align-self-<?php echo $align; ?>">
                            <?php if( have_rows('rechts') ): ?>
                                <?php while( have_rows('rechts') ): the_row(); ?>
                                    <?php include('main-fields.php'); ?>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php echo $narrow_close; ?>
            <?php echo $whitebox_close; ?>
        </div>
    </div>

<?php elseif( get_row_layout() == 'partners' ): ?>

    <?php
        $num = get_sub_field('aantal_weergeven_per_pagina');
        $layout = get_sub_field('layout');

        if($layout=='12'){
            $cols = 'col-md-12';
        }elseif($layout=='6'){
            $cols = 'col-md-12 col-lg-6';
        }elseif($layout=='4'){
            $cols = 'col-md-6 col-lg-4';
        }elseif($layout=='3'){
            $cols = 'col-md-6 col-lg-3';
        }
    ?>

    <?php if($num!=''){
            $posts_pp = $num;
        }else{
            $posts_pp = 12;
        }

        if ( get_query_var( 'paged' ) ) { $paged = get_query_var( 'paged' ); }
        elseif ( get_query_var( 'page' ) ) { $paged = get_query_var( 'page' ); }
        else { $paged = 1; }
	?>

    <div class="section grey">

        <div class="graph">
            <?php echo file_get_contents(get_template_directory_uri()."/images/section_graph.svg");?>
        </div>

        <div class="container">

            <?php $meta_filter = array('relation' => 'OR');

                if (!empty($_POST)){

                    if(isset($_POST['nederland'])){
                        
                        $_SESSION['nederland'] = $_POST['nederland'];
                        $meta_filter[] = array('key' =>'land','value' => $_POST['nederland'],'compare' => '=');

                    }else{
                        unset($_SESSION['nederland']);
                    }

                    if(isset($_POST['belgie'])){
                        
                        $_SESSION['belgie'] = $_POST['belgie'];
                        $meta_filter[] = array('key' =>'land','value' => $_POST['belgie'],'compare' => '=');

                    }else{
                        unset($_SESSION['belgie']);
                    }

                    if(empty($_POST['nederland']) && empty($_POST['belgie'])){
                        unset($_SESSION['nederland']);
                        unset($_SESSION['belgie']);
                        $meta_filter[] = '';
                        echo '<script>alert("unset");</script>';
                    }

                }else{
                    if(!empty($_SESSION['nederland'])){
                        $meta_filter[] = array('key' =>'land','value' => $_SESSION['nederland'],'compare' => '=');
                    }
                    if(!empty($_SESSION['belgie'])){
                        $meta_filter[] = array('key' =>'land','value' => $_SESSION['belgie'],'compare' => '=');
                    }
                }
            ?>

            <div class="row">
                <div class="col-md-4">
                    <div class="filter">
                        <form id="partner-filter" method="post" action="<?php echo get_the_permalink(); ?>">
                            <h4>Toon partners uit:</h4>
                            <?php
                                if(!empty($_SESSION['nederland'])){
                                    $checked = ' checked'; 
                                }else{
                                    $checked = '';
                                }

                                if(!empty($_SESSION['belgie'])){
                                    $checked2 = ' checked'; 
                                }else{
                                    $checked2 = '';
                                }   
                            ?>
                            <div class="form-check">
							<input class="form-check-input" type="checkbox" name="nederland" value="nl" <?php echo $checked; ?>>
							<label class="form-check-label">Nederland</label>
							</div>

                            <div class="form-check">
							<input class="form-check-input" type="checkbox" name="belgie" value="be" <?php echo $checked2; ?>>
							<label class="form-check-label">Belgie & Luxemburg</label>
							</div>

                        </form>
                    </div>
                </div>
            </div>

            <?php
                $args = array (
                    'posts_per_page' => $num,
                    'paged' => $paged,
                    'orderby' => 'menu_order',
                    'order' => 'ASC',
                    'post_type' => 'partner',
                    'post_status' => 'publish',
                    'meta_query' => $meta_filter
                );
            ?>

            <?php query_posts($args); ?>
            <?php if(have_posts()): ?>
                <div class="row">
                    <?php while(have_posts()): the_post(); ?>

                        <?php $logo = get_field('logo'); ?>

                        <div class="<?php echo $cols; ?>">
                            <a href="<?php the_permalink(); ?>" class="partner-item">

                                <div class="graph2">
                                    <?php echo file_get_contents(get_template_directory_uri()."/images/section_graph.svg");?>
                                </div>

                                <h3><?php the_title(); ?></h3>
                                <p>
                                <?php the_field('straat_huisnummer'); ?><br />
                                <?php the_field('postcode'); ?> <?php the_field('plaats'); ?><br />
                                </p>
                                <p class="outlined">
                                <?php if(get_field('telefoonnummer')){ ?><i class="fas fa-phone"></i> <?php the_field('telefoonnummer'); ?><br /><?php } ?>
                                <?php if(get_field('emailadres')){ ?><i class="fas fa-envelope"></i> <?php the_field('emailadres'); ?><br /><?php } ?>
                                <?php if(get_field('website')){ ?><i class="fas fa-globe"></i> <?php the_field('website'); ?><?php } ?>
                                </p>
                            </a>
                        </div>
                    <?php endwhile; ?>
                </div>
                <div class="pagination-holder">
                    <?php
                        if (function_exists("fellowtuts_wpbs_pagination"))
                        {
                            fellowtuts_wpbs_pagination();
                            //fellowtuts_wpbs_pagination($the_query->max_num_pages);
                        }
                    ?>
                </div>
            <?php else : ?>
                <p>Geen partners gevonden!</p>
            <?php endif; wp_reset_query(); ?>

        </div>

    </div>

<?php elseif( get_row_layout() == 'voorgedefineerd_blok' ): ?>

    <?php $contentblok = get_sub_field('contentblok'); ?>
    <?php $post = $contentblok; ?>

    <?php setup_postdata($post); ?>

        <?php include('predefined-fields.php'); ?>

    <?php wp_reset_postdata(); ?>
        
<?php endif; ?>