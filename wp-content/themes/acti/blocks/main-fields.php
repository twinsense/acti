<?php if( have_rows('inhoud') ): ?>
	<?php while ( have_rows('inhoud') ) : the_row(); $count2++;?>
		<?php if( get_row_layout() == 'titel' ): ?>

			<?php $h = get_sub_field('heading'); ?>
			<<?php echo $h; ?>><?php the_sub_field('titel'); ?></<?php echo $h; ?>>

		<?php elseif( get_row_layout() == 'tekst' ): ?>

			<?php the_sub_field('tekst'); ?>

		<?php elseif( get_row_layout() == 'afbeelding' ): ?>

			<?php
				$image = get_sub_field('afbeelding');
				$stijl = get_sub_field('stijl');
			?>

			<?php if( !empty($image) ): ?>
				<?php if($image['subtype']!='gif'){ ?>
					<?php if($stijl=='normal'){ ?>
						<img class="content-image" src="<?php echo $image['sizes']['gallery']; ?>" alt="<?php echo $image['alt']; ?>" />
					<?php }else{ ?>
						<div class="image arrow" style="background-image:url('<?php echo $image['sizes']['gallery']; ?>');">
						</div>
					<?php } ?>
				<?php }else{ ?>
					<img class="content-image" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				<?php } ?>
			<?php endif; ?>

		<?php elseif( get_row_layout() == 'video' ): ?>

			<div class="embed-container">
				<?php the_sub_field('video'); ?>
			</div>

		<?php elseif( get_row_layout() == 'fotogalerij' ): ?>

			<?php

			$columns = get_sub_field('layout');
			$images = get_sub_field('fotogalerij');

			if( $images ): ?>
				<div class="row">
					<?php foreach( $images as $image ): ?>
						<div class="col-sm-6 col-md-4">
							<a href="<?php echo $image['sizes']['gallery']; ?>" class="gallery-image" style="background-image: url(<?php echo $image['sizes']['small-thumbnail']; ?>);">
							</a>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>

		<?php elseif( get_row_layout() == 'producten_spotlight' ): ?>

			<?php $products = get_sub_field('producten'); ?>

			<div class="slider-holder product-slider-holder">

				<div id="product-slider-btn-left" class="slider-button btn-left"><?php echo file_get_contents(get_template_directory_uri()."/images/arrow_left.svg");?></div>
				<div id="product-slider-btn-right" class="slider-button btn-right"><?php echo file_get_contents(get_template_directory_uri()."/images/arrow_right.svg");?></div>

				<div class="product-slider">
				<?php if( $products ): ?>
					<?php foreach( $products as $post ): ?>
					<?php setup_postdata($post); ?>

						<?php $attachment_image = wp_get_attachment_url( get_post_thumbnail_id(), 'gallery' ); ?>
						
						<a href="<?php echo get_the_permalink(); ?>" class="product-slide">
							<div class="inner">
								<div class="product-slide-image" style="background-image:url(<?php echo $attachment_image; ?>);">
								</div>
								<h4><?php the_title(); ?></h4>
							</div>
						</a>

					<?php endforeach; ?>
					<?php wp_reset_postdata(); ?>
				<?php endif; ?>
				</div>
			</div>

		<?php elseif( get_row_layout() == 'overzicht_paginas' ): ?>

			<?php 
				$pages = get_sub_field('paginas');
				$columns = get_sub_field('layout');

				if($columns=='12'){
					$col = 'col-12';
				}elseif($columns=='6'){
					$col = 'col-12 col-sm-6';
				}elseif($columns=='4'){
					$col = 'col-12 col-sm-6 col-md-4';
				}elseif($columns=='3'){
					$col = 'col-12 col-sm-6 col-md-3';
				}elseif($columns=='2'){
					$col = 'col-12 col-sm-4 col-md-3 col-lg-2';
				}
			?>

			<?php if( $pages ): ?>
				<div class="row">
					<?php foreach( $pages as $post ): ?>
					<?php setup_postdata($post); ?>

						<div class="<?php echo $col; ?>">

							<?php $attachment_image = wp_get_attachment_url( get_post_thumbnail_id(), 'gallery' ); ?>

							<a href="<?php echo get_the_permalink(); ?>" class="pagina-item" style="background-image:url(<?php echo $attachment_image; ?>);">
								<div class="overlay">
									<h3><?php the_title(); ?></h3>
								</div>
							</a>
						</div>

					<?php endforeach; ?>
				</div>
				<?php wp_reset_postdata(); ?>
			<?php endif; ?>

		<?php elseif( get_row_layout() == 'products' ): ?>

			<?php $producten = get_sub_field('pages'); ?>

			<div class="row">
				<?php if( $producten ): ?>
					<?php foreach( $producten as $post ): ?>
						<?php setup_postdata($post); ?>

						<div class="col-sm-6">
							<div class="overview-product-item">
								<div class="row">
									<div class="col-sm-5 align-self-center">
										<?php $logo = get_field('productfoto'); ?>
										<?php if( !empty($logo) ): ?>
											<img src="<?php echo $logo['sizes']['gallery']; ?>" alt="<?php echo $logo['alt']; ?>" />
										<?php endif; ?>
									</div>
									<div class="col-sm-7 align-self-center">
										<h4><?php the_title(); ?></h4>
										<p><?php the_field('samenvatting'); ?></p>
										<a href="<?php the_permalink(); ?>" class="btn btn-primary">Meer informatie</a>
									</div>
								</div>
							</div>
						</div>

					<?php endforeach; ?>
					<?php wp_reset_postdata(); ?>
				<?php endif; ?>
			</div>

		<?php elseif( get_row_layout() == 'nieuws-overzicht' ): ?>

			<div class="nieuwsberichten">
				<div class="row">
					<?php $cats = get_sub_field('categorieen'); ?>
					<?php $num = get_sub_field('hoeveel_weergeven');

						$args = array (
							'tax_query' => array(
								array(
									'taxonomy' => 'category',
									'field' => 'id',
									'terms' => $cats
								)
							),
							'posts_per_page' => $num,
							'orderby' => 'date',
							'post_type' => 'post',
							'post__not_in' => array(get_the_id()),
							'post_status' => 'publish'
						);

						query_posts($args);
						if(have_posts()): ?>
						<?php while(have_posts()):the_post(); ?>

							<?php
								$id = get_post_thumbnail_id();
								$thumbnail = wp_get_attachment_image_src( $id, 'small-thumbnail' );
							?>

							<div class="col-lg-6">
								<a class="nieuws-item wow animate__animated animate__fadeInUp" href="<?php echo get_the_permalink(); ?>">
									<div class="nieuws-item-image" style="background-image:url(<?php echo $thumbnail[0]; ?>);">
									</div>
									<div class="nieuws-item-text height1">
										<h3><?php the_title(); ?></h3>
										<span><?php echo get_the_date('d - m - Y'); ?>
										<?php
											$terms = get_the_terms($post,'category');
											foreach($terms as $term){
												echo '</span><span class="category">'.$term->name.'</span>';
											}
										?>
									</div>
								</a>
							</div>

						<?php endwhile; ?>
					<?php endif; wp_reset_query(); ?>
				</div>
			</div>

		<?php elseif( get_row_layout() == 'usps' ): ?>

			<?php if( have_rows('usps') ): ?>
				<ul class="usps fa-ul">
					<?php while ( have_rows('usps') ) : the_row(); ?>
						<li><span class="fa-li"><i class="fas fa-check"></i></span><?php the_sub_field('tekst'); ?></li>	
					<?php endwhile; ?>
				</ul>
			<?php endif;?>

		<?php elseif( get_row_layout() == 'downloads' ): ?>

			<?php
				$columns = get_sub_field('layout');

				if($columns=='12'){
					$col = 'col-6';
				}elseif($columns=='6'){
					$col = 'col-6 col-sm-6';
				}elseif($columns=='4'){
					$col = 'col-6 col-sm-6 col-md-4';
				}elseif($columns=='3'){
					$col = 'col-6 col-sm-6 col-md-3';
				}elseif($columns=='2'){
					$col = 'col-6 col-sm-4 col-md-3 col-lg-2';
				}
			?>

			<?php if( have_rows('downloads') ): ?>
				<div class="downloads">
					<div class="row justify-content-center">
						<?php while ( have_rows('downloads') ) : the_row(); ?>
							<div class="<?php echo $col; ?>">
								<div class="download">

									<?php if(get_sub_field('titel')){ ?>
										<h4><?php the_sub_field('titel'); ?></h4>
									<?php } ?>

									<?php
									$link = get_sub_field('link');
									if( $link ):
										$link_url = $link['url'];
										$link_target = $link['target'] ? $link['target'] : '_self';
										?>
										<a class="btn btn-primary" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">Download</a>
									<?php endif; ?>

								</div>
							</div>
						<?php endwhile; ?>
					</div>
				</div>
			<?php endif;?>

		<?php elseif( get_row_layout() == 'knoppen' ): ?>

			<?php if( have_rows('knoppen') ): ?>
				<?php while ( have_rows('knoppen') ) : the_row(); ?>

					<?php $style = get_sub_field('stijl');

					$link = get_sub_field('link');
						if( $link ):
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
						?>
						<a class="btn <?php echo $style; ?>" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
					<?php endif; ?>

				<?php endwhile; ?>
			<?php endif;?>
			<div class="clearfix"></div>

		<?php endif; ?>

	<?php endwhile; ?>
<?php endif; ?>
