<?php
	get_header();
?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php if ( is_front_page() ) :
    include('blocks/headerslider.php');
else :
    include('blocks/headerslider-page.php');
endif; ?>

<div class="content">
	
    <div class="section white">
        <div class="container">
            <div class="row">
                <div class="col-xl-8">
                    <div class="row">
                        <div class="col-md-6">
                            <?php if(get_field('naam_bedrijf')){ ?>
                                <h1><?php the_field('naam_bedrijf'); ?></h1>
                            <?php }else{ ?>
                                <h1><?php the_title(); ?></h1>
                            <?php } ?>
                            <p>
                            <?php the_field('straat_huisnummer'); ?><br />
                            <?php the_field('postcode'); ?> <?php the_field('plaats'); ?>
                            </p>
                            <p class="outlined">
                            <?php if(get_field('telefoonnummer')){ ?><i class="fas fa-phone"></i> <a href="tel:<?php the_field('telefoonnummer'); ?>"><?php the_field('telefoonnummer'); ?></a><br /><?php } ?>
                            <?php if(get_field('emailadres')){ ?><i class="fas fa-envelope"></i> <a href="mailto:<?php the_field('emailadres'); ?>"><?php the_field('emailadres'); ?></a><br /><?php } ?>
                            <?php if(get_field('website')){ ?><i class="fas fa-globe"></i> <a target="_blank" href="<?php the_field('website'); ?>"><?php the_field('website'); ?></a><?php } ?>
                            </p>
                        </div>
                        <div class="col-md-6">
                            <?php if(get_field('google_maps')){ ?>
                                <div class="embed-container">
                                    <?php the_field('google_maps'); ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if( have_rows('secties') ): ?>
		<?php while ( have_rows('secties') ) : the_row(); ?>
		
			<?php include('blocks/sections.php'); ?>
			
		<?php endwhile; ?>
		
	<?php endif; ?>

</div>

<?php endwhile; else : echo '<p>No content</p>'; endif; ?>

<?php
 	get_footer();
?>
