<?php
	get_header();
?>

<div class="content">
	
		<?php
		$archives = array();
		$postitems = array();
		$pageitems = array();
		$productitems = array();
		?>

		<?php if (have_posts()) :
			while (have_posts()) :
			the_post();

				if($post->post_type == 'post'){
					$postitems[] = $post;
				}elseif($post->post_type == 'page'){
					$pageitems[] = $post;
				}elseif($post->post_type == 'product'){
					$productitems[] = $post;
				}

			endwhile;
		else : ?>
		<div class="section white">
			<div class="container">
				<h1>Geen zoekresultaten gevonden voor <span>"<?php echo get_search_query(); ?>"</span></h1>
				<p>Onze excuses. Helaas heeft uw opgegeven zoekterm geen resultaten opgeleverd.</p>
			</div>
		</div>

		<?php endif; ?>

		<?php
			/*
			echo '<p>paginas</p><br>';
			print_r($pageitems);

			echo '<p>producten</p><br>';
			print_r($productitems);

			echo '<p>support</p><br>';
			print_r($supportitems);

			echo '<p>PDF-documenten</p><br>';
			print_r($attachmentitems);

			echo '<p>term-paginas</p><br>';
			print_r($termitems);
			*/
			
			//print_r($attachmentitems);
			
		?>

		<?php if($postitems || $pageitems || $productitems){ ?>
			<div class="section yellow small-padding">
				<div class="container">
					<h1>Zoekresultaten voor <span>"<?php echo get_search_query(); ?>"</span>:</h1>
				</div>
			</div>
		<?php } ?>
		
		<?php if($productitems){ ?>
			<div class="section grey">
				
				<div class="graph">
					<?php echo file_get_contents(get_template_directory_uri()."/images/section_graph.svg");?>
				</div>

				<div class="container">
					<h2>Producten</h2>
					<div class="row">
						<?php foreach($productitems as $post){ ?>
							<div class="col-md-3">

								<?php $attachment_image = get_the_post_thumbnail_url($post, 'gallery'); ?>
						
								<a href="<?php echo get_the_permalink(); ?>" class="product-item">
									<div class="inner">
										<div class="product-item-image" style="background-image:url(<?php echo $attachment_image; ?>);">
										</div>
										<h4><?php the_title(); ?></h4>
									</div>
								</a>

							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		<?php } ?>
		
		<?php if($pageitems){ ?>
			<div class="section white">
				<div class="container">
					<h2>Pagina's</h2>
					<div class="row">
						<?php foreach($pageitems as $post){ ?>
							<div class="col-md-4">

								<?php $attachment_image = get_the_post_thumbnail_url($post, 'gallery'); ?>

								<a href="<?php echo get_the_permalink(); ?>" class="pagina-item" style="background-image:url(<?php echo $attachment_image; ?>);">
									<div class="overlay">
										<h3><?php the_title(); ?></h3>
									</div>
								</a>

							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		<?php } ?>
		
		<?php if($documentatieitems){ ?>
			<div class="section grey">
				<div class="container">
					<h2>Handleidingen</h2>
					<div class="row">
						<?php foreach($documentatieitems as $post){ ?>
							<div class="col-md-4">
								<div class="pdf-item">
									<h3><?php echo get_the_title($post->ID); ?></h3>
									<?php /*echo get_post_type(); */ ?>
									<?php echo get_field('omschrijving', $post->ID); ?>
									<?php if(get_field( 'bestand', $post->ID )){ ?>
										<a class="pdf-btn btn btn-primary" target="_blank" href="<?php echo get_field( 'bestand', $post->ID ); ?>"><i class="fas fa-file-pdf"></i> Download PDF</a>
									<?php } ?>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		<?php } ?>

		<?php if($postitems){ ?>
			<div class="section white">
				<div class="container">
					<h1><span>Berichten</span></h1>
					<div class="row">
						<?php foreach($postitems as $post){ ?>
							<div class="col-md-4">

								<?php $image = get_the_post_thumbnail_url($post, 'gallery'); ?>

								<?php if( !empty($image) ): ?>
									<div class="page-item-image" style="background-image: url(<?php echo $image; ?>);">
									</div>
								<?php endif; ?>
								<div class="item">
									<div class="post-item">
										<div class="row">
											<div class="col-12 text">
												<?php if(get_field('titel')){ ?>
													<h3><?php the_field('titel'); ?></h3>
												<?php }else{ ?>
													<h3><?php the_title(); ?></h3>
												<?php } ?>

												<?php if(get_field('subtitel')){ ?>
													<h4><?php the_field('subtitel'); ?></h4>
												<?php } ?>

												<b><?php echo get_the_date('d-m-Y'); ?></b>

												<?php the_field('samenvatting'); ?>

												<a class="btn btn-primary" href="<?php the_permalink(); ?>">Lees meer</a>
											</div>
										<?php the_field('icoon'); ?>
										</div>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		<?php } ?>
</div>

<?php
 	get_footer();
?>
