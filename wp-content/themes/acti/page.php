<?php
	get_header();
?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php if ( is_front_page() ) :
    include('blocks/headerslider.php');
else :
    include('blocks/headerslider-page.php');
endif; ?>

<div class="content">

	<?php if( have_rows('secties') ): ?>
		<?php while ( have_rows('secties') ) : the_row(); ?>
		
			<?php include('blocks/sections.php'); ?>
			
		<?php endwhile; ?>
		
	<?php else : echo '<div class="section white"><div class="container"><p>Nog geen inhoud toegevoegd op pagina.</p></div></div>'; ?>
		
	<?php endif; ?>
</div>

<?php endwhile; else : echo '<p>No content</p>'; endif; ?>

<?php
 	get_footer();
?>
