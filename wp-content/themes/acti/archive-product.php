<?php get_header(); ?>

<div class="content">
	<div class="container">
        <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
			<?php if(function_exists('bcn_display')){
				bcn_display();
			}?>
		</div>
	</div>
	<div class="section white">
		<div class="container">
		
			<h1><?php post_type_archive_title(); ?></h1>
			
			<?php 
			
			$layout = get_field('layout', 'option');
			if($layout=='12'){
				$cols = 'col-12';
			}elseif($layout=='6'){
				$cols = 'col-12 col-md-6 col-lg-6';
			}elseif($layout=='4'){
				$cols = 'col-12 col-md-6 col-lg-4';
			}elseif($layout=='3'){
				$cols = 'col-12 col-md-6 col-lg-3';
			}
			
			$terms = get_field('categorieen_producten', 'option');
			
			if( $terms ): ?>
			
				<div class="row">
				
					<?php $count1 = 0; ?>
					
					<?php foreach( $terms as $term ): $count1 = $count1+0.1; ?>
					
                        <div class="<?php echo $cols; ?>">

                            <?php $image = get_field('afbeelding', $term); ?>

                            <a href="<?php echo get_term_link( $term ); ?>" class="pagina-item" style="background-image:url(<?php echo $image['sizes']['gallery']; ?>);">
                                <div class="overlay">
                                    <h3><?php echo $term->name; ?></h3>
                                </div>
                            </a>

						</div>
				
					<?php endforeach; ?>
			
				</div>
			
			<?php endif; ?>
		</div>			
	</div>	
</div>

<?php get_footer(); ?>
