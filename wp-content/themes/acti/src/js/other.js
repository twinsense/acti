// Import jQuery
import $ from 'jquery';
import 'slick-carousel';
import 'webpack-jquery-ui';

var ajax = jquery.ajax;
console.log(ajax);

var bumpIt = function() {
      $('body').css('margin-bottom', $('.footer').height());
      $('.header-slide-image').css('padding-right', (($(window).width() - $('.container').width())/2));
      $('.header-slide-image').css('margin-right', (-($(window).width() - $('.container').width())/2));
      $('.header-slide-image .overlay').css('padding-right', (($(window).width() - $('.container').width())/2));
      $('.header-slide-image .overlay').css('margin-right', (-($(window).width() - $('.container').width())/2));
      $('.product-slide').css('min-height', $('.product-slide').width());
	  $('.product-item').css('min-height', $('.product-item').width());
    },
    didResize = false;

bumpIt();

$(window).resize(function() {
  didResize = true;
});
setInterval(function() {
  if(didResize) {
    didResize = false;
    bumpIt();
  }
}, 250);

$(document).ready(function() {

	$("input:checkbox").change(function(){
		$("#product-filter").submit();
	});

	$("#partner-filter input:checkbox").change(function(){
		$("#partner-filter").submit();
	});

	$("#product-filter select").change(function(){
		$("#product-filter").submit();
	});

	$(".filter-expander").click(function(){
		$(".filter-sections").slideToggle(500);
	});

  
	$('.header-slider').slick({
		dots: false,
		arrows: true,
		infinite: true,
		adaptiveHeight: false,
		prevArrow: $('#header-slider-btn-left'),
		nextArrow: $('#header-slider-btn-right'),
		speed: 500,
		fade: true,
		cssEase: 'linear',
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 5000
	});
	
	$('.product-slider').slick({
		dots: false,
		arrows: true,
		infinite: true,
		prevArrow: $('#product-slider-btn-left'),
		nextArrow: $('#product-slider-btn-right'),
		speed: 500,
		cssEase: 'linear',
		autoplay: true,
		autoplaySpeed: 5000,
		slidesToShow: 4,
		slidesToScroll: 1,
		rows: 0,
		responsive: [
			{
			breakpoint: 1200,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					infinite: true,
					dots: false
				}
			},
			{
			breakpoint: 768,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					infinite: true,
					dots: false
				}
			},
			{
			breakpoint: 576,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					infinite: true,
					dots: false
				}
			}
		]
	});

	$('.header-slide').matchHeight({byRow:true});
	$('.product-item .inner').matchHeight({byRow:true});
	$('.partner-item').matchHeight({byRow:true});

  	var url_string = window.location.href;
	var url = new URL(url_string);
  	var active_tab = url.searchParams.get("tab");
	//alert(active_tab);
	
	if(active_tab){
		$('#nav-btn-'+active_tab).addClass('active');
		$('.tab-section').hide();
		$('#'+active_tab).show("fade", { direction: "up" }, "slow");
	}else{
		$( ".tab-btn" ).first().addClass('active');
		$('.tab-section').hide();
		$('#beschrijving').show("fade", { direction: "up" }, "slow");
	}
	
	$( ".tab-btn" ).click(function() {

    var id = $(this).data('sectie');

		$('.tab-btn').removeClass('active');
		$(this).addClass('active');
		
		$('.tab-section').hide();
		$('#'+id).show("fade", { direction: "up" }, "slow");
		
		function updateURLParameter(url, param, paramVal){
			var newAdditionalURL = "";
			var tempArray = url.split("?");
			var baseURL = tempArray[0];
			var additionalURL = tempArray[1];
			var temp = "";
			if (additionalURL) {
				tempArray = additionalURL.split("&");
				for (var i=0; i<tempArray.length; i++){
					if(tempArray[i].split('=')[0] != param){
						newAdditionalURL += temp + tempArray[i];
						temp = "&";
					}
				}
			}
		
			var rows_txt = temp + "" + param + "=" + paramVal;
			return baseURL + "?" + newAdditionalURL + rows_txt;
		}
		
		var newURL = updateURLParameter(window.location.href, 'locId', 'newLoc');
		newURL = updateURLParameter(newURL, 'resId', 'newResId');
		window.history.replaceState('', '', updateURLParameter(window.location.href, "tab", id));
		
	});

  bumpIt();
	
});
