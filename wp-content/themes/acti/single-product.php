<?php
	get_header();
?>

<?php include('blocks/header-product.php'); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<div class="content">
    <div class="container">
        <div class="section-tabs">
            <div class="tab-btn" id="nav-btn-beschrijving" data-sectie="beschrijving">Beschrijving<span><?php echo file_get_contents(get_template_directory_uri()."/images/arrow_left.svg");?></span></div>
            <!--<div class="tab-btn" id="nav-btn-specificaties" data-sectie="specificaties">Specificaties<span><?php echo file_get_contents(get_template_directory_uri()."/images/arrow_left.svg");?></span></div>-->
            <div class="tab-btn" id="nav-btn-downloads" data-sectie="downloads">Downloads<span><?php echo file_get_contents(get_template_directory_uri()."/images/arrow_left.svg");?></span></div>
            <a class="tab-btn right" target="_blank" href="http://twitter.com/share?text=<?php the_title(); ?>&url=<?php the_permalink(); ?>"><i class="fab fa-twitter"></i></a>
            <a class="tab-btn right" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>"><i class="fab fa-facebook-f"></i></a>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="tab-section" id="beschrijving">

        <?php if( have_rows('secties') ): ?>
            <?php while ( have_rows('secties') ) : the_row(); ?>
            
                <?php include('blocks/sections.php'); ?>
                
            <?php endwhile; ?>
            
        <?php else : echo '<div class="container"><p>Nog geen inhoud toegevoegd op pagina.</p></div>'; ?>
            
        <?php endif; ?>
    </div>
    <div class="tab-section" id="specificaties">
        <div class="section white">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <?php if( have_rows('specificatietabel', $object->ID) ): ?>
                            <table class="table">
                            <?php while ( have_rows('specificatietabel', $object->ID) ) : the_row(); ?>
                                <?php if( get_row_layout('cameras_algemeen')): ?>
                                    <thead class="thead-title">
                                        <tr>
                                        <td><?php the_sub_field('titel'); ?></td>
                                        <td></td>
                                        </tr>
                                    </thead>
                                    <?php if( have_rows('rijen') ): ?>
                                        <tbody>
                                            <?php while ( have_rows('rijen') ) : the_row(); ?>
                                                <tr>
                                                    <td>
                                                        <?php the_sub_field('label'); ?>
                                                    </td>
                                                    <td class="td-value">
                                                        <?php if(get_sub_field('waarde')){ ?>
                                                            <?php the_sub_field('waarde'); ?>
                                                        <?php } ?>
                                                        <?php if(get_sub_field('janee')=='ja'){
                                                            echo '<span class="icon-green"><i class="fas fa-check"></i></span>';
                                                        }elseif(get_sub_field('janee')=='nee'){
                                                            echo '<span class="icon-grey"><i class="fas fa-minus"></i></span>';
                                                        }else{
                                                            echo '';
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                            <?php endwhile; ?>
                                        </tbody>
                                    <?php endif;?>
                                <?php endif; ?>
                            <?php endwhile; ?>
                            </table>
                        <?php else : ?>
                            <div class="thead-title-idle">
                                Nog geen specificaties ingevoerd!
                            </div>
                        <?php endif;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-section" id="downloads">
        <div class="section white">
            <div class="container">

                <?php
                    $columns = get_field('layout_downloads');

                    if($columns=='12'){
                        $col = 'col-6';
                    }elseif($columns=='6'){
                        $col = 'col-6 col-sm-6';
                    }elseif($columns=='4'){
                        $col = 'col-6 col-sm-6 col-md-4';
                    }elseif($columns=='3'){
                        $col = 'col-6 col-sm-6 col-md-3';
                    }elseif($columns=='2'){
                        $col = 'col-6 col-sm-4 col-md-3 col-lg-2';
                    }
                ?>

                <?php if( have_rows('downloads') ): ?>
                    <div class="row">
                        <?php while ( have_rows('downloads') ) : the_row(); ?>
                            <div class="<?php echo $col; ?>">
                                <div class="download-section">
                                    <h3><?php the_sub_field('titel'); ?></h3>

                                    <?php $downloads = get_sub_field('download_items');
                                        if( $downloads ): ?>
                                        <?php foreach( $downloads as $download ):

                                            $title = get_field( 'titel', $download->ID ); ?>

                                            <div class="download-item">
                                               <h4><?php echo $title; ?></h4>

                                                <?php if( have_rows('releases', $download->ID) ): ?>
                                                    <table class="table">
                                                        <?php while( have_rows('releases', $download->ID) ): the_row(); ?>

                                                            <?php $type = get_sub_field('type'); ?>

                                                            <tr>
                                                                <td><?php the_sub_field('versienummer'); ?></td>

                                                                <?php if($type=='file'){ ?>
                                                                    <td><a target="_blank" class="btn btn-primary" href="<?php the_sub_field('bestand'); ?>">Downloaden</a></td>
                                                                <?php }else{ ?>
                                                                    <td><a target="_blank" class="btn btn-primary" href="<?php the_sub_field('link'); ?>">Downloaden</a></td>
                                                                <?php } ?>
                                                            </tr>
                                                        <?php endwhile; ?>
                                                    </table>
                                                <?php endif; ?>

                                            </div>

                                        <?php endforeach; ?>
                                    <?php endif; ?>

                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>
                <?php endif;?>
            </div>
        </div>
    </div>
</div>

<?php endwhile; else : echo '<p>No content</p>'; endif; ?>

<?php
 	get_footer();
?>
