<?php
	get_header();
?>

<div class="content">

	<div class="section grey">

		<div class="container">
			<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
				<?php if(function_exists('bcn_display')){
					bcn_display();
				}?>
			</div>
		</div>

		<div class="graph">
			<?php echo file_get_contents(get_template_directory_uri()."/images/section_graph.svg");?>
		</div>

		<div class="container">
			<h1>404 pagina niet gevonden!</h1>
            <p>De inhoud die u zoekt bestaat niet of is verplaatst.</p>
		</div>
	</div>
</div>

<?php
 	get_footer();
?>
