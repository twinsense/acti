<?php

function adminStyles() {
	wp_enqueue_style('acf-admin-styles' , get_template_directory_uri().'/acf-admin-style.css');
}

function loadingStyles() {
	wp_enqueue_style('frontend.css', get_stylesheet_directory_uri() . '/public/frontend.css', array());
	wp_enqueue_style('style', get_stylesheet_uri());
}

function loadingScripts() {
	wp_register_script('frontend-bundle.js', get_template_directory_uri() . '/public/frontend-bundle.js', array('jquery'),'1.1', true);
	wp_enqueue_script('frontend-bundle.js');
  wp_localize_script(
    'jquery',
    'jquery',
    array(
      'ajax' => admin_url( 'admin-ajax.php' ),
      'nonce' => wp_create_nonce('acti')
    )
  );
}

function themeSetup() {

// Widgets
register_sidebar( array(
	'name'				=> __( 'Footer 1', 'acti' ),
	'id'				=> 'footer1',
	'before_widget' 	=> '<div class="footer-section">',
	'after_widget'		=> '</div>'
));

register_sidebar( array(
	'name'				=> __( 'Footer 2', 'acti' ),
	'id'				=> 'footer2',
	'before_widget' 	=> '<div class="footer-section">',
	'after_widget'		=> '</div>'
));

register_sidebar( array(
	'name'				=> __( 'Footer 3', 'acti' ),
	'id'				=> 'footer3',
	'before_widget' 	=> '<div class="footer-section">',
	'after_widget'		=> '</div>'
));

register_sidebar( array(
	'name'				=> __( 'Footer 4', 'acti' ),
	'id'				=> 'footer4',
	'before_widget' 	=> '<div class="footer-contact">',
	'after_widget'		=> '</div>'
));

// Menu support
	add_theme_support( 'menus' );

// Featured image support
	add_theme_support('post-thumbnails');
	add_image_size('small-thumbnail', 600, 600, false);
	add_image_size('gallery', 900, 900, false );
	add_image_size('header', 2000, 2000, false );
}

// Register scripts
add_action('admin_head', 'adminStyles');
add_action('wp_enqueue_scripts', 'loadingStyles');
add_action('wp_enqueue_scripts', 'loadingScripts');
add_action('after_setup_theme', 'themeSetup');

// SVG
function my_myme_types($mime_types){
    $mime_types['svg'] = 'image/svg+xml'; //Adding svg extension
    return $mime_types;
}

add_filter('upload_mimes', 'my_myme_types', 1, 1);

// Remove actions
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

// Remove text area
add_action('admin_init', 'remove_textarea');
function remove_textarea() {
	remove_post_type_support( 'post', 'editor' );
	remove_post_type_support( 'page', 'editor' );
}

// // Register menu
// register_nav_menu('primary', 'Main menu');

/**
 * Register Custom Navigation Walker
 */

 register_nav_menus( array(
    'primary' => __( 'Hoofdmenu', 'acti' ),
) );

function register_navwalker(){
	require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';
}

add_action( 'after_setup_theme', 'register_navwalker' );


// Intented to use with locations, like 'primary'
// clean_custom_menu("primary");

function menu_mobile( $theme_location ) {
    if ( ($theme_location) && ($locations = get_nav_menu_locations()) && isset($locations[$theme_location]) ) {
        $menu = wp_get_nav_menu_object( $locations[$theme_location], 'primary' );
        $menu_items = wp_get_nav_menu_items($menu->term_id);

        $menu_list .= '<ul class="main-nav-mobile">' ."\n";

        $count = 0;
        $submenu = false;

        foreach( $menu_items as $menu_item ) {

            $link = $menu_item->url;
            $title = $menu_item->title;

            if ( !$menu_item->menu_item_parent ) {
                $parent_id = $menu_item->ID;

                $menu_list .= '<li class="nav-item">' ."\n";
                $menu_list .= '<a href="'.$link.'">'.$title.'</a>' ."\n";
            }

            if ( $parent_id == $menu_item->menu_item_parent ) {

                if ( !$submenu ) {
                    $submenu = true;

                    $title_parent = wp_get_post_parent_id($menu_item);

                    $menu_list .= '<span data-subm="'.$parent_id.'" class="sub-drop-m"><i class="fas fa-chevron-right"></i></span>';
                    $menu_list .= '<div id="submenu-m-'.$parent_id.'" class="sub-menu-m">' ."\n";
                    $menu_list .= '<div class="row">' ."\n";
                    $menu_list .= '<div class="sub-nav-item sub-drop-nested col-12"><span data-subm="'.$parent_id.'" class="sub-drop-m"><i class="fas fa-chevron-left"></i></span>'.get_the_title($title_parent).'</div>';
                }

                $menu_list .= '<div class="sub-nav-item col-md-6 col-lg-4 col-xl-3">' ."\n";
                $menu_list .= '<a href="'.$link.'">'.$title.'</a>' ."\n";
                $menu_list .= '</div>' ."\n";


                if ( $menu_items[ $count + 1 ]->menu_item_parent != $parent_id && $submenu ){
                	$menu_list .= '</div>' ."\n";
                    $menu_list .= '</div>' ."\n";
                    $submenu = false;
                }

            }

            if ( $menu_items[ $count + 1 ]->menu_item_parent != $parent_id ) {
                $menu_list .= '</li>' ."\n";
                $submenu = false;
            }

            $count++;
        }

        $menu_list .= '</ul>' ."\n";

    } else {
        $menu_list = '<!-- no menu defined in location "'.$theme_location.'" -->';
    }
    echo $menu_list;
}

function menu_desktop( $theme_location ) {
    if ( ($theme_location) && ($locations = get_nav_menu_locations()) && isset($locations[$theme_location]) ) {
        $menu = wp_get_nav_menu_object( $locations[$theme_location], 'primary' );
        $menu_items = wp_get_nav_menu_items($menu->term_id);

        $menu_list .= '<ul class="main-nav">' ."\n";

        $count = 0;
        $submenu = false;

        foreach( $menu_items as $menu_item ) {

            $link = $menu_item->url;
            $title = $menu_item->title;

            if ( !$menu_item->menu_item_parent ) {
                $parent_id = $menu_item->ID;

                $menu_list .= '<li class="nav-item">' ."\n";
                $menu_list .= '<a href="'.$link.'" class="title">'.$title.'</a>' ."\n";
            }

            if ( $parent_id == $menu_item->menu_item_parent ) {

                if ( !$submenu ) {
                    $submenu = true;
                    $menu_list .= '<span data-sub="'.$parent_id.'" class="sub-drop"><i class="fas fa-caret-down"></i></span>';
                    $menu_list .= '<div id="submenu-'.$parent_id.'" class="sub-menu">' ."\n";
                    $menu_list .= '<div class="row">' ."\n";
                }

                $menu_list .= '<div class="sub-nav-item col-md-6 col-lg-4 col-xl-3">' ."\n";
                $menu_list .= '<a href="'.$link.'" class="title">'.$title.'</a>' ."\n";
                $menu_list .= '</div>' ."\n";


                if ( $menu_items[ $count + 1 ]->menu_item_parent != $parent_id && $submenu ){
                	$menu_list .= '</div>' ."\n";
                    $menu_list .= '</div>' ."\n";
                    $submenu = false;
                }

            }

            if ( $menu_items[ $count + 1 ]->menu_item_parent != $parent_id ) {
                $menu_list .= '</li>' ."\n";
                $submenu = false;
            }

            $count++;
        }

        $menu_list .= '</ul>' ."\n";

    } else {
        $menu_list = '<!-- no menu defined in location "'.$theme_location.'" -->';
    }
    echo $menu_list;
}

function get_browser_user()
{
    $browser = '';
    $ua = strtolower($_SERVER['HTTP_USER_AGENT']);
    if (preg_match('~(?:msie ?|trident.+?; ?rv: ?)(\d+)~', $ua, $matches)) $browser = 'ie ie'.$matches[1];
    elseif (preg_match('~(safari|chrome|firefox)~', $ua, $matches)) $browser = $matches[1];

    return $browser;
}

add_action( 'init', 'create_post_type' );
function create_post_type() {
  register_post_type( 'contentblok',
    array(
      'menu_icon' => 'dashicons-editor-kitchensink',
      'labels' => array(
        'name' => __( 'Contentblokken' ),
        'singular_name' => __( 'Contentblok' ),
        'edit_item' => __( 'Contentblok aanpassen' ),
        'new_item' => __( 'Nieuw Contentblok' ),
        'edit_item' => __( 'Contentblok aanpassen' ),
        'add_new_item' => __( 'Contentblok toevoegen' ),
        'add_new' => __( 'Contentblok toevoegen' ),
        'view_item' => __( 'Contentblok bekijken' ),
      ),
      'public' => true,
      'publicly_queryable' => false,
      'has_archive' => false,
      'hierarchical' => false,
      'show_in_nav_menus' => true,
      'supports' => array('title', 'page-attributes')
    )
  );
  register_post_type( 'download',
    array(
      'menu_icon' => 'dashicons-download',
      'labels' => array(
        'name' => __( 'Downloads' ),
        'singular_name' => __( 'Download' ),
        'edit_item' => __( 'Download aanpassen' ),
        'new_item' => __( 'Nieuw Download' ),
        'edit_item' => __( 'Download aanpassen' ),
        'add_new_item' => __( 'Download toevoegen' ),
        'add_new' => __( 'Download toevoegen' ),
        'view_item' => __( 'Download bekijken' ),
      ),
      'public' => true,
      'publicly_queryable' => false,
      'has_archive' => false,
      'hierarchical' => false,
      'show_in_nav_menus' => true,
      'supports' => array('title', 'page-attributes')
    )
  );
  register_post_type( 'product',
    array(
      'menu_icon' => 'dashicons-cart',
      'labels' => array(
        'name' => __( 'Producten' ),
        'singular_name' => __( 'Product' ),
        'edit_item' => __( 'Product aanpassen' ),
        'new_item' => __( 'Nieuwe Product' ),
        'edit_item' => __( 'Product aanpassen' ),
        'add_new_item' => __( 'Product toevoegen' ),
        'add_new' => __( 'Product toevoegen' ),
        'view_item' => __( 'Product bekijken' ),
      ),
      'public' => true,
      'has_archive' => true,
      'hierarchical' => true,
      'show_in_nav_menus' => true,
	  'publicly_queryable' => true,
	  'query_var' => true,
	  'can_export' => true,
	  'exclude_from_search' => false,
      'rewrite' => array('slug' => 'producten'),
      'supports' => array('title', 'thumbnail', 'page-attributes')
    )
  );
  register_post_type( 'partner',
    array(
      'menu_icon' => 'dashicons-groups',
      'labels' => array(
        'name' => __( 'Partners' ),
        'singular_name' => __( 'Partner' ),
        'edit_item' => __( 'Partner aanpassen' ),
        'new_item' => __( 'Nieuwe Partner' ),
        'edit_item' => __( 'Partner aanpassen' ),
        'add_new_item' => __( 'Partner toevoegen' ),
        'add_new' => __( 'Partner toevoegen' ),
        'view_item' => __( 'Partner bekijken' ),
      ),
      'public' => true,
      'has_archive' => false,
      'hierarchical' => true,
      'show_in_nav_menus' => true,
	  'publicly_queryable' => true,
	  'query_var' => true,
	  'can_export' => true,
	  'exclude_from_search' => false,
      'rewrite' => array('slug' => 'partner'),
      'supports' => array('title', 'thumbnail', 'page-attributes')
    )
  );
}

add_action( 'init', 'create_categorieen' );

function create_categorieen() {
  register_taxonomy(
		'product_cat',
		array('product'),
		array(
			'label' => __( 'Productcategorie' ),
			'hierarchical' => true,
			'query_var' => true,
			'show_in_nav_menus' => true,
			'show_admin_column' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'categorie' ),
		)
	);

	register_taxonomy(
		'download_cat',
		array('download'),
		array(
			'label' => __( 'Downloads' ),
			'hierarchical' => true,
			'query_var' => true,
			'show_in_nav_menus' => true,
			'show_admin_column' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'downloadcategorie' ),
		)
	);

  register_taxonomy(
		'partner_cat',
		array('partner'),
		array(
			'label' => __( 'Categorie' ),
			'hierarchical' => true,
			'query_var' => true,
			'show_in_nav_menus' => true,
			'show_admin_column' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'partnercategorie' ),
		)
	);

}

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
        'page_title'    => 'Producten Overzicht',
        'menu_title'    => 'Producten Overzicht',
        'menu_slug'     => 'options_product',
        'capability'    => 'edit_posts',
        'parent_slug'   => 'edit.php?post_type=product',
        'position'      => false,
        'icon_url'      => 'dashicons-images-alt2',
        'redirect'      => false,
    ));

	acf_add_options_page(
		array(
			'page_title'    => 'Opties',
			'menu_title'    => 'Opties',
			'menu_slug'     => 'options_opties'
		)
	);
}

/* pagination */

function fellowtuts_wpbs_pagination($pages = '', $range = 2)
{
 $showitems = ($range * 2) + 1;
 global $paged;
 if(empty($paged)) $paged = 1;
 if($pages == '')
 {
 global $wp_query;
 $pages = $wp_query->max_num_pages;

 if(!$pages)
 $pages = 1;
 }

 if(1 != $pages)
 {
     echo '<nav aria-label="Pagina navigatie" role="navigation">';
        echo '<span class="sr-only">Pagina navigatie</span>';

        /* echo '<li class="page-item disabled hidden-md-down d-none d-lg-block"><span class="page-link">Pagina '.$paged.' van '.$pages.'</span></li>'; */
 echo '<div class="row justify-content-center">';

 echo '<div class="col-12 col-md-3 text-center">';
 if($paged > 2 && $paged > $range+1 && $showitems < $pages)
 echo '<a class="btn btn-secondary" href="'.get_pagenum_link(1).'" aria-label="Eerste pagina"><i class="fas fa-angle-double-left"></i></a>';

 if($paged > 1 && $showitems < $pages)
 echo '<a class="btn btn-secondary" href="'.get_pagenum_link($paged - 1).'" aria-label="Vorige pagina"><i class="fas fa-angle-left"></i></a>';

 echo '<ul class="pagination ft-wpbs">';
 for ($i=1; $i <= $pages; $i++)
 {
 if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
 	 echo ($paged == $i)? '<li class="page-item active"><span class="page-link"><span class="sr-only">Huidige pagina </span>'.$i.'</span></li>' : '<li class="page-item"><a class="page-link" href="'.get_pagenum_link($i).'"><span class="sr-only">Pagina </span>'.$i.'</a></li>';
 }
 echo '</ul>';

 if ($paged < $pages && $showitems < $pages)
 echo '<a class="btn btn-secondary" href="'.get_pagenum_link($paged + 1).'" aria-label="Volgende pagina"><i class="fas fa-angle-right"></i></a>';

 if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages)
 echo '<a class="btn btn-secondary" href="'.get_pagenum_link($pages-1).'" aria-label="Laatste pagina"><i class="fas fa-angle-double-right"></i></a>';
 echo '</div>';
 echo '</div>';
        echo '</nav>';
        //echo '<div class="pagination-info mb-5 text-center">[ <span class="text-muted">Page</span> '.$paged.' <span class="text-muted">of</span> '.$pages.' ]</div>';
 }
}

?>
