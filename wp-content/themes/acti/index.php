<?php
	get_header();
?>

<div class="content">

	<div class="container">
		<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
			<?php if(function_exists('bcn_display')){
				bcn_display();
			}?>
		</div>
	</div>

	<div class="section grey">

		<div class="graph">
			<?php echo file_get_contents(get_template_directory_uri()."/images/section_graph.svg");?>
		</div>

		<div class="container">

			<h1><span>Nieuws</span></h1>

			<div class="row">

				<?php if($num!=''){
						$posts_pp = $num;
					}else{
						$posts_pp = 12;
					}

					if ( get_query_var( 'paged' ) ) { $paged = get_query_var( 'paged' ); }
					elseif ( get_query_var( 'page' ) ) { $paged = get_query_var( 'page' ); }
					else { $paged = ''; }
				?>

				<?php
				$args = array (
					'posts_per_page' => $num,
					'paged' => $paged,
					'orderby' => 'menu_order',
					'order' => 'ASC',
					'post_status' => 'publish',
				);

				$query = new WP_Query($args); ?>

				<?php $count = 0; ?>
				<?php if (have_posts()) : while (have_posts()) : the_post(); $count++; ?>

					<div class="col-12 col-md-6">

						<?php $attachment_image = wp_get_attachment_url( get_post_thumbnail_id(), 'gallery' ); ?>

						<div class="news-item">
							<div class="row">
								<div class="col-md-5 align-self-center">
									<div class="image" style="background-image:url(<?php echo $attachment_image; ?>);">
									</div>
								</div>
								<div class="col-md-7 align-self-center">
									<div class="text-holder">
										<h3><?php the_title(); ?></h3>
										<?php if(get_field('toon_datum')){ ?>
											<p><?php echo get_the_date('d-m-Y'); ?></p>
										<?php } ?>
										<a class="btn btn-primary" href="<?php the_permalink(); ?>">Lees meer</a>
									</div>
								</div>
							</div>
						</div>
					</div>

				<?php endwhile; else : echo '<p>No content</p>'; endif; ?>
			</div>
			<div class="pagination-holder">

				<?php
					if (function_exists("fellowtuts_wpbs_pagination"))
					{
						fellowtuts_wpbs_pagination();
						//fellowtuts_wpbs_pagination($the_query->max_num_pages);
					}
				?>

			</div>
		</div>
	</div>
</div>

<?php
 	get_footer();
?>
