<?php session_start(); ?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title><?php wp_title(''); ?></title>
		<link rel="preconnect" href="https://fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;600;700;800&display=swap" rel="stylesheet">
		<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
		<link rel="manifest" href="/site.webmanifest">
		<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#584c9d">
		<meta name="msapplication-TileColor" content="#584c9d">
		<meta name="theme-color" content="#ffffff">
		<?php wp_head(); ?>
	</head>

	<body <?php body_class(); ?>>

		<header class="menu-holder">

			<!-- Navigation -->

			<nav class="navbar navbar-expand-md navbar-light bg-light" role="navigation">
			  <div class="container">

					<!-- Toggler -->

					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu-container" aria-controls="menu-container" aria-expanded="false" aria-label="<?php esc_attr_e( 'Toggle navigation', 'your-theme-slug' ); ?>">
						<span class="navbar-toggler-icon icon-open"><i class="fas fa-bars"></i></span>
						<span class="navbar-toggler-icon icon-close"><i class="fas fa-times"></i></span>
					</button>

					<!-- Logo -->

					<div class="logo-holder">
						<a class="navbar-brand" href="<?php echo get_site_url().'/'; ?>">
							<img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="Logo" />
						</a>
					</div>

					<!-- Hoofdmenu -->

					<?php
					wp_nav_menu( array(
						'theme_location'    => 'primary',
						'depth'             => 2,
						'container'         => 'div',
						'container_class'   => 'collapse navbar-collapse',
						'container_id'      => 'menu-container',
						'menu_class'        => 'nav navbar-nav',
						'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
						'walker'            => new WP_Bootstrap_Navwalker(),
					) );
					?>

					<!-- Search bar -->

					<div class="search-bar">
						<?php get_search_form(); ?>
			    </div>
				</nav>
			</header>
